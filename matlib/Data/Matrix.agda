{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings

module Data.Matrix (sr : CommSemiRing)  where

open import Data.Vec hiding (zipWith; map)
open import Data.Nat renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality
open import Data.Product hiding (map) renaming (_×_ to _⊗_ )
open module sr = CommSemiRing sr

infixl 8 _⋆_
infixl 7 _⊹_

{- matrix of n rows and m columns -}
_×_ : ℕ → ℕ → Set
n × m  = Vec (Vec R m) n


zipWith : ∀ {n} {A : Set} {B : Set} {C : Set} →
          (A → B → C) → Vec A n → Vec B n → Vec C n
zipWith f [] [] = []
zipWith f (x ∷ xs) (y ∷ ys) = f x y ∷ zipWith f xs ys


map :  {n : ℕ}{A B : Set} → (A → B) → Vec A n → Vec B n
map f  []   =  []
map f (x ∷ xs)=  f x ∷ map f xs


{- matrix transposition -}
〈_〉  : {n m : ℕ} → m × n → n × m
〈 [] 〉 = replicate []
〈 xs ∷ xss 〉  = zipWith _∷_ xs 〈 xss 〉


{- make 0-mtrx matrix  -}
0-mtrx : {m n : ℕ} → m × n
0-mtrx {m} {n} = replicate (replicate zero)



{- add matrices -}
_⊹_ : {n m : ℕ} → m × n → m × n → m × n
_⊹_ [] [] = []
_⊹_ (x ∷ xs) (y ∷ ys) = zipWith _+_ x y ∷ xs ⊹ ys 



{- matrix multiplication -}
multRow : {n m : ℕ} → Vec R n → m × n → Vec R m
multRow v [] = []
multRow v (x ∷ xs) =
  (foldr _ (_+_) zero (zipWith (_*_) v x )) ∷ multRow v xs


_⋆_ : {n m k : ℕ} → m × n → n × k → m × k
_⋆_ [] _ = []
_⋆_ (x ∷ xs) m2 = multRow x 〈 m2 〉 ∷ xs ⋆  m2



{- putting matrices together -}
_◫_ : {n m k : ℕ} → k × n → k × m 
  → k × (n ✚ m)
xss ◫ yss = zipWith _++_ xss yss 

_++++_ : {n m k : ℕ} → k × n → k × m
  → k × (n ✚ m)
m1 ++++ m2 = m1 ◫ m2



{- identity matrix -}
id : {n : ℕ} → n × n
id {0} = []
id {suc n} = (one ∷ replicate zero) ∷ zipWith _++_ (0-mtrx {n} {1})  (id {n})


{- blocks extension: (matrices as arrows) -}
i₁ : {n p : ℕ} → (n ✚ p) × n
i₁ {n} {p} = (id {n}) ++ (0-mtrx {p} {n})

i₂ : {n p : ℕ} → (n ✚ p) × p
i₂ {n} {p} = (0-mtrx {n} {p}) ++ id {p}

π₁ : {n p : ℕ} → n × (n ✚ p)
π₁ {n} {p} = zipWith _++_ (id {n}) (0-mtrx {n} {p})

π₂ : {n p : ℕ} → p × (n ✚ p)
π₂ {n} {p} = zipWith _++_ (0-mtrx {p} {n})  (id {p})

⟦_/_⟧ : {n p m : ℕ} → (n × m) → (p × m) → (n ✚ p) × m
⟦_/_⟧ {n} {p} {m}  U V  = i₁ {n} {p} ⋆ U ⊹ i₂ {n} {p} ⋆ V

⟦_∣_⟧ : {k l m : ℕ} → (k × l) → (k × m) → k × (l ✚ m)
⟦_∣_⟧ {k} {l} {m} R S = R ⋆ π₁ ⊹ (S ⋆ π₂ {l})
