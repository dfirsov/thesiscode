{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings

module Data.Matrix.MultIdentProperties (csr : CommSemiRing) where

open import Data.Vec hiding (zipWith; map)
open import Data.Nat renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality
open module csr = CommSemiRing csr

import Data.Matrix; open Data.Matrix csr
import Data.Matrix.MultZeroProperties; open Data.Matrix.MultZeroProperties csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr


m-zeroi*1 : {m : ℕ} 
  →  multRow {nzero} {m} [] (replicate []) ≡ replicate zero
m-zeroi*1 {nzero} = refl
m-zeroi*1 {suc n} = cong (_∷_ zero) m-zeroi*1

{- id ⋆ xss ≡ xss -}

m-one*0 : {n m : ℕ}(xs : Vec R m)(vs : m × (suc n)) 
  → multRow (one ∷ zero ∷ replicate zero) (zipWith _∷_ xs vs) ≡ xs
m-one*0 [] [] = refl
m-one*0 {n} (x ∷ xs) (y ∷ ys)
  rewrite law6 {x} 
  | m-zero*4 y  
  | m-zero*3 {n} {zero} 
  | law3 {zero} 
  | law3 {x} = cong (_∷_ x) (m-one*0 _ ys)


m-one*1 : {n m : ℕ}(x : Vec R m)(xs : n × m) 
  → multRow (one ∷ replicate zero) (zipWith _∷_ x  〈 xs 〉) ≡ x
m-one*1 [] [] = refl
m-one*1 [] ([] ∷ xs) with (zipWith _∷_ [] (zipWith _∷_ [] 〈 xs 〉))
m-one*1 [] ([] ∷ xs) | [] = refl
m-one*1 (x ∷ xs) [] 
  rewrite law6 {x} 
  | law3 {x} = cong (_∷_ x) (m-one*1 xs [])
m-one*1 {suc n} {suc m} (x ∷ xs) (x' ∷ xs') 
  with  (zipWith _∷_ x' 〈 xs' 〉)
... | v ∷ vs 
  rewrite law6 {x} 
  | m-zero*4 v  
  | m-zero*3 {n} {zero} 
  | law3 {zero} 
  | law3 {x} = cong (_∷_ x) (m-one*0 xs vs) 


m-one*3 : {n : ℕ}(xs : Vec R n) 
  → multRow (zero ∷ []) (zipWith _∷_ xs (replicate [])) ≡ 
    replicate zero
m-one*3 [] = refl
m-one*3 (x ∷ xs) 
  rewrite law10 {x} 
  | law2 {zero} 
  = cong (_∷_ _) (m-one*3 xs)


m-one*4 : {n m : ℕ}(xs : Vec R n)(ys : Vec R m)(yss : m × n) 
  → multRow (zero ∷ xs) (zipWith _∷_ ys yss) ≡ multRow xs yss
m-one*4 xs [] [] = refl
m-one*4 (xs) (y ∷ ys) (zs ∷ zss) 
  rewrite law10 {y} 
  | law2 {foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs)}
  = cong (_∷_ _) (m-one*4 xs ys zss)


m-one*2 : {n m k : ℕ}(xs : Vec R k)(vss : m × n)(xss : n × k) 
  → (zipWith _++_ (replicate (zero ∷ [])) vss) ⋆ (xs ∷ xss) 
    ≡ vss ⋆ xss
m-one*2 xs [] yss = refl
m-one*2 {nzero} {suc m} {k} xs ([] ∷ vss) [] 
  rewrite m-zeroi*1 {k} 
  | m-one*3 {k} xs 
  = cong (_∷_ _) (m-one*2 xs vss [])
m-one*2 {suc n} {suc m} {k} xs (vs ∷ vss) (ys ∷ yss) 
  rewrite m-one*4 {suc n} {k} vs xs
    (zipWith _∷_ ys 〈 yss 〉)
  = cong (_∷_ _) (m-one*2 xs vss (ys ∷ yss))


m-one* : {k l : ℕ}(m : k × l)
  →  id ⋆ m ≡ m
m-one* [] = refl
m-one* {suc m} {n} (xs ∷ xss)
  rewrite m-one*1 {m} {n} xs xss 
  | m-one*2 {m} {m} {n} xs (id {m} ) xss = cong (_∷_ _) (m-one* xss)


{- xss ⋆ id ≡ xss -}
m-*one11 : {k : ℕ}(xs : Vec R k)
  → foldr (λ _ → R) _+_ zero
      (zipWith _*_ xs (replicate zero)) ≡ zero
m-*one11 [] = refl
m-*one11 {suc k} (x ∷ xs) 
  rewrite law11 {x}
  | m-*one11 {k} xs 
  | law2 {zero} = refl

m-*one12 : {k l : ℕ}(x : R)(xs : Vec R k)(m1 : l × k)
  → multRow (x ∷ xs) (zipWith _∷_ (replicate zero) m1) ≡ 
    multRow xs m1
m-*one12 x xs []  = refl
m-*one12 x xs (x0 ∷ xs') 
  rewrite law11 {x}
  | law2 
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs x0)} 
  = cong (_∷_ _) (m-*one12 x xs xs')


m-*one1 : {k : ℕ}(x : Vec R k)
  → multRow x id ≡ x
m-*one1 [] = refl
m-*one1 {suc k} (x ∷ xs) 
  rewrite law7 {x}
  | m-*one11 xs
  | law3 {x} 
  | id-transpose1 {k} id 
  | m-*one12 x xs id = cong (_∷_ _) (m-*one1 xs)


m-*one : {k l : ℕ}(m : k × l)
  → m ⋆ id ≡ m
m-*one [] = refl
m-*one {suc m} {n}  (x ∷ xs)
  rewrite (〈id〉 {n})
  | m-*one xs
  | m-*one1 x = refl
