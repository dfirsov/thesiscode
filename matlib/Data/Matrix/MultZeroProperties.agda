{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings

module Data.Matrix.MultZeroProperties (csr : CommSemiRing) where
open import Data.Vec hiding (zipWith; map)
open import Data.Nat renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality
open module csr = CommSemiRing csr

import Data.Matrix; open Data.Matrix csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr


{- PROPERTY: 0 * m = 0 -}

m-zero*1 : {n m : ℕ}(x : n × m)
  → multRow (replicate zero) x ≡ replicate zero
m-zero*1 [] = refl
m-zero*1 {suc n} {m} (x ∷ xs) 
  rewrite m-zero*4 x 
  | m-zero*3 {m} {zero} = cong (_∷_ _) (m-zero*1 xs)


m-zero* : {l n k : ℕ}(m : n × k)
  →  (0-mtrx {l}) ⋆ m ≡ 0-mtrx
m-zero* {nzero} xs  = refl
m-zero* {suc n'} xs
  rewrite m-zero*1 〈 xs  〉
  | m-zero* {n'} xs
  = refl 




{- xss ⋆ 0-mtrx  ≡ 0-mtrx -}
m-*zero11 : {m : ℕ}
  → multRow {nzero} {m} [] (replicate [])  ≡ replicate zero
m-*zero11 {nzero} = refl
m-*zero11 {suc n} = cong (_∷_ _) m-*zero11


m-*zero1 : {n m : ℕ}(x : Vec R n) 
  → multRow x (0-mtrx {m} )
  ≡ replicate zero
m-*zero1 [] = m-*zero11
m-*zero1 {suc n} {nzero} (x ∷ xs) = refl
m-*zero1 {suc n} {suc n'} (x ∷ xs)
  rewrite law11 {x} 
  | law2 {foldr (λ _ → R) _+_ zero
      (zipWith _*_ xs (replicate zero))} 
  | m-zero*41 xs
  | m-zero*3 {n} {zero} = cong (_∷_ _) (m-*zero1 {suc n} {n'} (x ∷ xs))


m-*zero : {k l n : ℕ}(m : l × n)
  →  m ⋆ (0-mtrx {n} {k}) ≡ 0-mtrx
m-*zero [] = refl
m-*zero {k} {suc m} {n}   (x ∷ xs) 
   rewrite 〈0-mtrx〉 n k
   | m-*zero {k} {m} {n} xs 
   | m-*zero1 {_} {k} x = refl 



