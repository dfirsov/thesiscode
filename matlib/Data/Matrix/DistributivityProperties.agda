{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings

module Data.Matrix.DistributivityProperties (csr : CommSemiRing) where
open import Data.Vec renaming (zipWith to devnull; map to devnull2)
open import Data.Nat renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality
open module csr = CommSemiRing csr

import Data.Matrix; open Data.Matrix csr
import Data.Matrix.MultZeroProperties; open Data.Matrix.MultZeroProperties csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr



{- PROPERTY: (m1 ⊹ m2) ⋆ m3 ≡ ((m1 ⋆ m3) ⊹ (m2 ⋆ m3)) -}
mm-+*distribm1 : {k : ℕ}(x y z : Vec R k)
  → foldr (λ _ → R) _+_ zero (zipWith _*_ x z) +
      foldr (λ _ → R) _+_ zero (zipWith _*_ y z) ≡ 
  foldr (λ _ → R) _+_ zero (zipWith _*_ (zipWith _+_ x y) z)
mm-+*distribm1 [] [] [] rewrite law3 {zero} = refl
mm-+*distribm1 (x ∷ xs) (y ∷ ys) (z ∷ zs) 
  rewrite law1 {x * z}
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs)}
    {((y * z) + foldr (λ _ → R) _+_ zero (zipWith _*_ ys zs))}
  | sym (law1 
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs)}
    {y * z}
    {foldr (λ _ → R) _+_ zero (zipWith _*_ ys zs)})
  | sym (law1 
    {x * z}
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs) + (y * z)}
    {foldr (λ _ → R) _+_ zero (zipWith _*_ ys zs)})
  | law4
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs)}
    {y * z}
  | sym (law1
    {x * z}
    {y * z}
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs)})
  | law9 {x} {y} {z}
  | law1 {(x * z) + (y * z)}
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs)}
    {foldr (λ _ → R) _+_ zero (zipWith _*_ ys zs)}
  | mm-+*distribm1 xs ys zs
  = refl


mm-+*distribm : {k m : ℕ}(x y : Vec R k)(z : m × k)
  → zipWith _+_ (multRow x z) (multRow y z) ≡ 
    multRow (zipWith _+_ x y) z
mm-+*distribm x y [] = refl
mm-+*distribm x y (z ∷ zs) 
  rewrite mm-+*distribm x y zs 
  | mm-+*distribm1 x y z = refl


mm-+*distrib : {n m k : ℕ}(m₁ m₂ : n × m)(m₃ : m × k)
  → (m₁ ⊹ m₂) ⋆ m₃ ≡ ((m₁ ⋆ m₃) ⊹ (m₂ ⋆ m₃))
mm-+*distrib [] [] m3 = refl
mm-+*distrib (x ∷ xs) (y ∷ ys) m3 
  rewrite mm-+*distrib xs ys m3
  | mm-+*distribm x y 〈 m3 〉
  = refl


{- PROPERTY: m1 ⋆ (m2 ⊹ m3) ≡ ((m1 ⋆ m2) ⊹ (m1 ⋆ m3)) -}

mm-*+distribm1 : {k : ℕ}(x y z : Vec R k)
  → foldr (λ _ → R) _+_ zero (zipWith _*_ x y) +
    foldr (λ _ → R) _+_ zero (zipWith _*_ x z) ≡
    foldr (λ _ → R) _+_ zero
      (zipWith _*_ x (zipWith _+_ y z))
mm-*+distribm1 [] [] [] = law3 {zero}
mm-*+distribm1 (x ∷ xs) (y ∷ ys) (z ∷ zs) 
  rewrite law1
    {x * y} 
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs ys)} 
    {((x * z) + foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs))}
  | sym (law1 
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs ys)}
    {x * z}
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs)})
  | law4
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs ys)}
    {x * z}
  | law1
    {x * z}
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs ys)}
    {foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs)}
  | sym (law1 
    {x * y}
    {x * z}
    {(foldr (λ _ → R) _+_ zero (zipWith _*_ xs ys) +
        foldr (λ _ → R) _+_ zero (zipWith _*_ xs zs))})
  | law8 {x} {y} {z}
  | mm-*+distribm1 xs ys zs
  = refl

mm-*+distribm : {k l : ℕ}(x : Vec R l)(y z : k × l)
  → zipWith _+_ (multRow x y) (multRow x z) ≡ 
    multRow x  (y ⊹ z)
mm-*+distribm x [] [] = refl
mm-*+distribm x (y ∷ ys) (z ∷ zs) 
  rewrite mm-*+distribm x ys zs 
  | mm-*+distribm1 x y z = refl




m-*+distrib : {k n m : ℕ}(m₁ : k × n)(m₂ : n × m)(m₃ : n × m)
  →  m₁ ⋆ (m₂ ⊹ m₃) ≡ (m₁ ⋆ m₂) ⊹ (m₁ ⋆ m₃)
m-*+distrib [] m2 m3 = refl
m-*+distrib (x ∷ xs) m2 m3 
  rewrite m-*+distrib xs m2 m3
  | 〈m₁⊹m₂〉 m2 m3 
  | sym (mm-*+distribm x 〈 m2 〉 〈 m3 〉) = refl