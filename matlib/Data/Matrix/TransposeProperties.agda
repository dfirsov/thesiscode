{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings


module Data.Matrix.TransposeProperties (csr : CommSemiRing) where
open import Data.Vec hiding (zipWith; map)
open import Data.Nat 
  renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality

import Data.Matrix
open Data.Matrix csr

open module csr = CommSemiRing csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr



{- PROPERTY: transpose(transpose m) = m  -}
m-TT0 : ∀ {n}(xs : n × 0 ) 
  → replicate [] ≡ xs

m-TT0 [] = refl
m-TT0 ([] ∷ xs) = cong (_∷_ _) (m-TT0 xs)


m-TT1 : ∀{m n}(xss : m × n)(xs : Vec R m) 
  → 〈 zipWith _∷_ xs xss 〉  ≡ xs ∷  〈 xss 〉
m-TT1 [] [] = refl
m-TT1 (x ∷ xs) (x' ∷ xs') = 
  cong (zipWith _∷_ (x' ∷ x)) (m-TT1 xs xs')


〈〈m〉〉 : {k l : ℕ}(m : k × l) 
  → 〈 〈 m 〉 〉 ≡ m
〈〈m〉〉 {nzero} {nzero} [] = refl
〈〈m〉〉 {nzero} {suc n} [] = cong (zipWith _∷_ []) (〈〈m〉〉 {nzero} {n} [])
〈〈m〉〉 (l ∷ m) = trans (m-TT1 〈 m 〉 l) (cong (_∷_ _) (〈〈m〉〉 m))  

{- somle lemma1 -}

〈〉++◫-lemma1 : {l k : ℕ}(ys : k × l)
  → zipWith _++_ (replicate []) ys ≡ ys
〈〉++◫-lemma1 [] = refl
〈〉++◫-lemma1 (x ∷ xs) = cong (_∷_ _) (〈〉++◫-lemma1 xs)


〈〉++◫-lemma2 : {l k n : ℕ}(x : Vec R k)(xs : k × n)(ys : k × l)
  → zipWith _∷_ x (zipWith _++_ xs ys) ≡
      zipWith _++_ (zipWith _∷_ x xs) ys
〈〉++◫-lemma2 [] [] [] = refl
〈〉++◫-lemma2 (x ∷ xs) (y ∷ ys) (z ∷ zs) = cong (_∷_ _) (〈〉++◫-lemma2 xs ys zs)




〈〉++◫-lemma : {l k n : ℕ}(xs : l × k)(ys : n × k)
  → 〈 xs ++ ys 〉 ≡ 〈 xs 〉 ◫ 〈 ys 〉
〈〉++◫-lemma [] ys rewrite 〈〉++◫-lemma1 (〈 ys 〉) = refl
〈〉++◫-lemma (x ∷ xs) ys rewrite 〈〉++◫-lemma xs ys =  〈〉++◫-lemma2 x 〈 xs 〉 〈 ys 〉


+〈〉property1 : {k l : ℕ}(x y : Vec R l)(xs ys : l × k)
  → zipWith _∷_ (zipWith _+_ x y) (xs ⊹ ys) ≡
      zipWith _∷_ x xs ⊹ zipWith _∷_ y ys
+〈〉property1 [] [] [] [] = refl
+〈〉property1 (x ∷ xs) (x' ∷ xs') (x0 ∷ xs0) (x1 ∷ xs1) = cong (_∷_ _) (+〈〉property1 xs xs' xs0 xs1)

〈m₁⊹m₂〉 : {k l : ℕ}(m₁ m₂ : k × l)
  → 〈 m₁ ⊹ m₂ 〉 ≡ 〈 m₁ 〉 ⊹ 〈 m₂ 〉
〈m₁⊹m₂〉 {nzero} {nzero} [] [] = refl
〈m₁⊹m₂〉 {nzero} {suc n} [] [] 
  = cong (_∷_ _) (〈m₁⊹m₂〉 {nzero} {n} [] [])
〈m₁⊹m₂〉 (x ∷ xs) (y ∷ ys) 
  rewrite 〈m₁⊹m₂〉 xs ys = +〈〉property1 x y 〈 xs 〉 〈 ys 〉





{- identity ≡ 〈 identity 〉 -}
m-*transposem1 : {m n : ℕ}(x : Vec R m)(f : m × n)
  → 〈 zipWith _∷_ x f 〉 ≡ x ∷ 〈 f 〉
m-*transposem1 [] [] = refl
m-*transposem1 (x ∷ xs) (x' ∷ xs') 
  rewrite m-*transposem1 xs xs' = refl





id-transpose1 : {k l : ℕ}(m : k × l)
  → zipWith _++_ (replicate (zero ∷ [])) m
  ≡ zipWith _∷_ (replicate zero) m
id-transpose1 [] = refl
id-transpose1 (x ∷ xs) = cong (_∷_ _) (id-transpose1 xs)

〈id〉 : {k : ℕ} → 〈 id 〉 ≡ id {k}
〈id〉 {nzero} = refl
〈id〉 {suc n} 
  rewrite id-transpose1 {n} {n} id 
  | m-*transposem1 {n} (replicate zero) id 
  | (〈id〉 {n}) = refl


{- 〈 0-mtrx k l 〉 ≡ 0-mtrx l k -}
0-mtrx-transpose1 : (k l : ℕ)
  → zipWith _∷_ (replicate {_} {k} zero) (replicate {_} {k} (replicate {_} {l} zero)) ≡
      replicate {_} {k} (zero ∷ replicate {_} {l} zero)
0-mtrx-transpose1 nzero l = refl
0-mtrx-transpose1 (suc n) l = cong (_∷_ _) (0-mtrx-transpose1 n l)


〈0-mtrx〉 : (k l : ℕ)
  → 〈 0-mtrx {k} {l} 〉 ≡ 0-mtrx {l} {k}
〈0-mtrx〉 nzero k = refl
〈0-mtrx〉 (suc n)  nzero 
  rewrite 〈0-mtrx〉 n nzero = refl
〈0-mtrx〉 (suc n) (suc n') 
  rewrite 〈0-mtrx〉 (n) (suc n')
  = cong (_∷_ _) (0-mtrx-transpose1 n' n)





