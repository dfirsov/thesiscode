{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings

module Data.Matrix.AllProperties (csr : CommSemiRing) where
open import Data.Vec hiding (zipWith; map)
open import Data.Nat renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality
import Data.Matrix


open Data.Matrix csr
open module csr = CommSemiRing csr

import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.AdditionProperties; open Data.Matrix.AdditionProperties csr
import Data.Matrix.DistributivityProperties; open Data.Matrix.DistributivityProperties csr
import Data.Matrix.MultAssocProperties; open Data.Matrix.MultAssocProperties csr
import Data.Matrix.MultIdentProperties; open Data.Matrix.MultIdentProperties csr
import Data.Matrix.MultZeroProperties; open Data.Matrix.MultZeroProperties csr
import Data.Matrix.TransposeProperties2; open Data.Matrix.TransposeProperties2 csr

import Data.Matrix.Arrows.Transposition; open Data.Matrix.Arrows.Transposition csr 
import Data.Matrix.Arrows.Identity; open Data.Matrix.Arrows.Identity csr 
import Data.Matrix.Arrows.Addition; open Data.Matrix.Arrows.Addition csr 
import Data.Matrix.Arrows.BlockOperations; open Data.Matrix.Arrows.BlockOperations csr 
import Data.Matrix.Arrows.Equality; open Data.Matrix.Arrows.Equality csr 
import Data.Matrix.Arrows.Extraction; open Data.Matrix.Arrows.Extraction csr

