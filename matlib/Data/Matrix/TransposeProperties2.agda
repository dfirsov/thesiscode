{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings

module Data.Matrix.TransposeProperties2 (csr : CommSemiRing) where
open import Data.Vec hiding (zipWith; map)
open import Data.Nat renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality
open module csr = CommSemiRing csr

import Data.Matrix; open Data.Matrix csr
import Data.Matrix.MultZeroProperties; open Data.Matrix.MultZeroProperties csr
import Data.Matrix.MultIdentProperties; open Data.Matrix.MultIdentProperties csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr
import Data.Matrix.AdditionProperties; open Data.Matrix.AdditionProperties csr


{- 〈 m1 ⋆ m2 〉 ≡  〈 m2 〉 ⋆ 〈 m1 〉 -}
m-*transposem2 : {k : ℕ}(x x' : Vec R k)
  → (zipWith _*_ x x') ≡ (zipWith _*_ x' x)
m-*transposem2 [] [] = refl
m-*transposem2 (x ∷ xs) (x' ∷ xs') 
  rewrite law12 {x} {x'}
  = cong (_∷_ _) (m-*transposem2 xs xs')


m-*transposem : {k m n : ℕ}(x : Vec R m)(d : k × m)(f : m × n)
  →  zipWith _∷_ (multRow x d) (d ⋆ f) ≡ d ⋆ zipWith _∷_ x f
m-*transposem {nzero} {m} x [] f = refl
m-*transposem {suc k} {m} x (x' ∷ xs) f 
  rewrite m-*transposem x xs f
  | m-*transposem1 x f 
  = trans (cong (λ q → q ∷ xs ⋆ zipWith _∷_ x f) ((cong (λ q → q ∷ multRow x' 〈 f 〉) (cong (λ g → foldr (λ _ → R) _+_ zero g) ((m-*transposem2 x x')))))) refl


〈m₁⋆m₂〉 : {n m k : ℕ} → (m₁ : n × m)(m₂ : m × k) 
  → 〈 m₁ ⋆ m₂ 〉 ≡  〈 m₂ 〉 ⋆ 〈 m₁ 〉
〈m₁⋆m₂〉 {nzero} [] m2 
  rewrite m-*zero {nzero} 〈 m2 〉 = refl
〈m₁⋆m₂〉 (x ∷ xs) m2
  rewrite 〈m₁⋆m₂〉 xs m2 with 〈 m2 〉 | 〈 xs 〉 
... | d | f = m-*transposem x d f