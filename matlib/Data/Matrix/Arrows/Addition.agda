{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings


module Data.Matrix.Arrows.Addition (csr : CommSemiRing) where
open import Data.Vec 
  renaming (zipWith to devnull; map to devnull2)
open import Data.Nat 
  renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality

import Data.Matrix
open Data.Matrix csr

open module csr = CommSemiRing csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.TransposeProperties2; open Data.Matrix.TransposeProperties2 csr
import Data.Matrix.DistributivityProperties; open Data.Matrix.DistributivityProperties csr
import Data.Matrix.MultAssocProperties; open Data.Matrix.MultAssocProperties csr
import Data.Matrix.MultIdentProperties; open Data.Matrix.MultIdentProperties csr
import Data.Matrix.MultZeroProperties; open Data.Matrix.MultZeroProperties csr
import Data.Matrix.AdditionProperties; open Data.Matrix.AdditionProperties csr
import Data.Matrix.Arrows.Identity; open Data.Matrix.Arrows.Identity csr
import Data.Matrix.Arrows.Zero; open Data.Matrix.Arrows.Zero csr


⟦m₁|m₂⟧⊹⟦m₃|m₄⟧ : {k l m : ℕ}(A : k × l)(B : k × m)(C : k × l)(D : k × m)
  → ⟦ A ∣ B ⟧ ⊹ ⟦ C ∣ D ⟧ ≡ ⟦ A ⊹ C ∣ B ⊹ D ⟧
⟦m₁|m₂⟧⊹⟦m₃|m₄⟧ {k} {l} {m}  A B C D 
  rewrite ⊹-comm (A ⋆ π₁ {l} {m}) (B ⋆ π₂ {l} {m}) 
  | ⊹-assoc (A ⋆ π₁ {l} {m}) (C ⋆ π₁ {l} {m}) (D ⋆ π₂ {l} {m})
  | sym (⊹-assoc (B ⋆ π₂ {l} {m}) (A ⋆ π₁ {l} {m})  ⟦ C ∣ D ⟧)
  | ⊹-assoc (A ⋆ π₁ {l} {m}) (C ⋆ π₁ {l} {m}) (D ⋆ π₂ {l} {m})
  | sym (mm-+*distrib A C (π₁ {l} {m}))
  with (A ⊹ C) ⋆ (π₁ {l} {m})
... | a
  rewrite ⊹-comm a (D ⋆ π₂ {l} {m})
  | (⊹-assoc (B ⋆ π₂ {l} {m}) (D ⋆ π₂ {l} {m}) a)
  | sym (mm-+*distrib B D (π₂ {l} {m}))
  with (B ⊹ D) ⋆ (π₂ {l} {m})
... | b 
  = ⊹-comm b a


⟦m₁/m₂⟧⊹⟦m₃/m₄⟧ : {k l m : ℕ}(A : k × l)(B : m × l)(C : k × l)(D : m × l)
  → ⟦ A / B ⟧ ⊹ ⟦ C / D ⟧ ≡ ⟦ A ⊹ C / B ⊹ D ⟧ 
⟦m₁/m₂⟧⊹⟦m₃/m₄⟧ {k} {l} {m} A B C D 
  rewrite ⊹-comm (i₁ {k} {m} ⋆ A) (i₂ {k} {m} ⋆ B)
  | sym (⊹-assoc (i₂ {k} {m} ⋆ B) (i₁ {k} {m} ⋆ A) ⟦ C / D ⟧)
  | (⊹-assoc (i₁ {k} {m} ⋆ A) (i₁ {k} {m} ⋆ C) (i₂ {k} {m} ⋆ D))
  | sym (m-*+distrib (i₁ {k} {m}) A C)
  with (i₁ {k} {m}) ⋆ (A ⊹ C)
... | a
  rewrite ⊹-comm a (i₂ {k} {m} ⋆ D)
  | (⊹-assoc (i₂ {k} {m} ⋆ B) (i₂ {k} {m} ⋆ D) a)
  | sym (m-*+distrib (i₂ {k} {m}) B D)
  with (i₂ {k} {m}) ⋆  (B ⊹ D)
... | b
  = ⊹-comm b a
