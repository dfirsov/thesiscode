{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings


module Data.Matrix.Arrows.Transposition (csr : CommSemiRing) where
open import Data.Vec 
  renaming (zipWith to devnull; map to devnull2)
open import Data.Nat 
  renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality

import Data.Matrix
open Data.Matrix csr

open module csr = CommSemiRing csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.TransposeProperties2; open Data.Matrix.TransposeProperties2 csr


〈i₁〉 : (n p : ℕ) →  〈 i₁ {n} {p} 〉 ≡ π₁ {n} {p}
〈i₁〉 n p
  rewrite 〈〉++◫-lemma  (id {n}) (0-mtrx {p} {n})
  | 〈id〉 {n}
  | 〈0-mtrx〉 p n = refl 

  
〈i₂〉 : (n p : ℕ) →  〈 i₂ {n} {p} 〉 ≡ π₂ {n} {p}
〈i₂〉 n p 
  rewrite 〈〉++◫-lemma (0-mtrx {n} {p}) (id {p})
  | 〈id〉 {p}
  | 〈0-mtrx〉 n p = refl


〈π₁〉 : (n p : ℕ) →  〈 π₁ {n} {p} 〉 ≡ i₁ {n} {p}
〈π₁〉 n p 
  rewrite sym (〈i₁〉 n p)
  | 〈〈m〉〉 {n ✚ p} {n} (id ++ replicate (replicate zero)) = refl


〈π₂〉 : (n p : ℕ) →  〈 π₂ {n} {p} 〉 ≡ i₂ {n} {p}
〈π₂〉 n p
  rewrite sym (〈i₂〉 n p)
  | 〈〈m〉〉 (replicate {_} {n}  (replicate {_} {p} zero) ++ (id {p})) = refl


-- 〈⟦ R ∣ S ⟧ 〉 ≡  ⟦ 〈 R  〉 /  〈 S  〉⟧
〈⟦m₁|m₂⟧〉 : {k l m : ℕ}(R : k × l)(S : k × m)
  → 〈 ⟦  R  ∣  S  ⟧  〉 ≡  ⟦ 〈 R 〉 / 〈 S 〉 ⟧
〈⟦m₁|m₂⟧〉 {k} {l} {m} R S 
  rewrite 〈m₁⊹m₂〉 (R ⋆ π₁ {l} {m} ) (S ⋆ π₂ {l} {m}) 
  | 〈m₁⋆m₂〉 R (π₁ {l} {m})
  | 〈π₁〉 l m
  | 〈m₁⋆m₂〉 S (π₂ {l} {m})
  | 〈π₂〉 l m
  =  refl


