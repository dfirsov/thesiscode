{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings

module Data.Matrix.Arrows.Identity (csr : CommSemiRing) where
open import Data.Vec 
  renaming (zipWith to devnull; map to devnull2)
open import Data.Nat 
  renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality

import Data.Matrix
open Data.Matrix csr

open module csr = CommSemiRing csr


import Data.Matrix.MultZeroProperties; open Data.Matrix.MultZeroProperties csr
import Data.Matrix.MultIdentProperties; open Data.Matrix.MultIdentProperties csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr
import Data.Matrix.AdditionProperties; open Data.Matrix.AdditionProperties csr



multlemm11 : {m l : ℕ}(d : m × l)(x : Vec R l)(vec : Vec R m)
  → multRow (zero ∷ x) (zipWith _∷_ vec d)
  ≡ multRow x d
multlemm11 [] x [] = refl
multlemm11 (x ∷ xs) x' (x0 ∷ xs') 
  rewrite law10 {x0} 
  | law2 {foldr _ _+_ zero (zipWith _*_ x' x)} = cong (_∷_ _) (multlemm11 xs x' xs')


multlemm1 : {k l m : ℕ}(m1 : k × l)(m2 : l × m)(vec : Vec R m)
  →  (zipWith _∷_ (replicate zero) m1) ⋆ (vec ∷ m2) ≡ m1 ⋆ m2
multlemm1 [] m2 vec = refl
multlemm1 (x ∷ xs) m2 vec  
  rewrite multlemm11 〈 m2 〉 x vec
  | multlemm1 xs m2 vec
   = cong (_∷_ _) refl


multlemm2 : {k l m : ℕ}(m1 : k × l)(m2 : l × m)
  → m1 ⋆ (zipWith _∷_ (replicate zero) m2) ≡ zipWith _∷_ (replicate zero) (m1 ⋆ m2)
multlemm2 [] m2 = refl
multlemm2 {suc n} {l} (x ∷ xs) m2 
  rewrite m-*transposem1 (replicate zero) m2
  | m-zero*41 x
  | m-zero*3 {l} {zero} = cong (_∷_ _) (multlemm2 xs m2)


multlemm31 : (k l n : ℕ)(m1 : k × n)
  →  zipWith _∷_ (replicate {_}  zero)
      (zipWith _∷_ (replicate {_} {k} zero) m1 ++
       replicate {_} {l} (zero ∷ replicate {_} {n} zero))  ≡ 
     zipWith _∷_ (replicate zero) (zipWith _∷_ (replicate zero) (m1 ++ replicate (replicate zero)))
multlemm31 nzero nzero n [] = refl
multlemm31 nzero (suc n) n' [] = cong (_∷_ _) (multlemm31 nzero n n' [])
multlemm31 (suc k) l n (x ∷ xs) = cong (_∷_ _) (multlemm31 k l n xs)

multlemm32 : {k l n : ℕ}(m1 : k × l)
  → zipWith _∷_ (replicate {_} {k} zero)
      (zipWith _∷_ (replicate {_} {k} zero) m1)
      ++ replicate {_} {n} (zero ∷ replicate zero)
  ≡ zipWith _∷_ (replicate zero)
      (zipWith _∷_ (replicate zero) m1 ++
       replicate (replicate zero))
multlemm32 {nzero} {l} {nzero} [] = refl
multlemm32 {nzero} {l} {suc n} [] = cong (_∷_ _) (multlemm32 {nzero} {l} {n} [])
multlemm32 (x ∷ xs) = cong (_∷_ _) (multlemm32 xs)

multlemm3 : (n m : ℕ)
  → (zipWith _∷_ (replicate zero) id ++
       replicate {_} {m} (zero ∷ replicate {_} {n} zero)) ≡ 
    (zipWith _∷_ (replicate zero) (id ++
       replicate {_} {m} (replicate {_} {n} zero)))
multlemm3 nzero nzero = refl
multlemm3 nzero (suc n) = cong (_∷_ _) (multlemm3 nzero n)
multlemm3 (suc n) ( n') 
  rewrite id-transpose1 {n} {n} id 
  | multlemm31 n n' n (id)
  | sym (multlemm3 n n') = cong (_∷_ _) (multlemm32 (id {n}))

multlemm4 : {k l n : ℕ} (m1 : k × (l ✚ n))
  → map
       (λ x →
          foldr _ _+_ zero
            (zipWith _*_ (one ∷ replicate {_} {l} zero ++ replicate {_} {n} zero) x))
       (zipWith _∷_ (replicate zero) m1) ≡ replicate zero
multlemm4 [] = refl
multlemm4 {suc k} {l} {n} (x ∷ xs) 
  rewrite law11 {one}
  | replicatelemm {l} {n} zero
  | m-zero*4 x
  | m-zero*3 {l ✚ n} {zero}
  | law2 {zero}
  = cong (_∷_ _) (multlemm4 {k} {nzero} {l ✚ n} xs)

multlemm5 : {l k : ℕ} → (x : R)(v : Vec R k)(m : l × k)
  → multRow (x ∷ v) (zipWith _∷_ (replicate zero) m) 
  ≡ multRow v m
multlemm5 x [] [] = refl
multlemm5 x [] ([] ∷ xs) 
 rewrite law11 {x} 
 | law2 {zero} = cong (_∷_ _) (multlemm5 x [] xs)
multlemm5 x (x' ∷ xs) [] = refl
multlemm5 x (x' ∷ xs) (x0 ∷ xs') 
  rewrite law11 {x} 
  | law2 {foldr _ _+_ zero (zipWith _*_ (x' ∷ xs) x0)} = cong (_∷_ _) (multlemm5 x (x' ∷ xs) xs')

π₁i₁ : (n p : ℕ) → (π₁ {n} {p}) ⋆ (i₁ {n} {p}) ≡ id {n}
π₁i₁ nzero p = refl
π₁i₁ (suc n) p 
  rewrite id-transpose1 {n} {n} id 
  | 〈〉++◫-lemma (zipWith _∷_ (replicate {_} {n} zero) (id {n})) (0-mtrx {p} {suc n})
  | m-*transposem1 (replicate zero) (id {n})
  | (〈id〉 {n})
  | 〈0-mtrx〉 p (suc n)
  | law6 {one}
  | replicatelemm {n} {p} zero
  | m-zero*41 (replicate {_} {n ✚ p} zero)
  | m-zero*3 {n ✚ p} {zero}
  | law3 {one}
  | multlemm4 {n} {n} {p} (zipWith _++_ (id {n}) (replicate {_} {n} (replicate {_} {p} zero)))
  | concatsup212' ((zipWith _∷_ (replicate zero) id)) (0-mtrx {n} {p})  ((one ∷ replicate zero) ∷
       zipWith _∷_ (replicate zero) id) (0-mtrx {p} {suc n})
  | multlemm1  (id {n}) (zipWith _∷_ (replicate zero) id) ((one ∷ replicate zero))
  | m-zero* {n} {p} {suc n} (0-mtrx {p} {suc n})
  | m-one* (zipWith _∷_ (replicate {_} {n} zero) id)
  | ⊹-zeror ((zipWith _∷_ (replicate {_} {n} zero) id))
  | multlemm5 {n} {n ✚ p}  one (replicate zero) (zipWith _++_ (id ) (replicate {_}   (replicate {_}  zero)))
  | m-zero*1 {n} {n ✚ p} (zipWith _++_ id (replicate (replicate zero)))
 =  refl 






multlemm_ob111 : {l n : ℕ}(x x' : Vec R l)(y : Vec R n)
  → foldr _ _+_ zero 
          (zipWith _*_ (replicate {_} {n} zero ++ x') (y ++ x)) ≡ 
    foldr _ _+_ zero (zipWith _*_ x' x)
multlemm_ob111 {nzero} {nzero} [] [] [] = refl
multlemm_ob111 {nzero} {suc n} [] [] (y ∷ ys)
  rewrite multlemm_ob111 {nzero} {n} [] [] ys
  | law10 {y}
  | law2 {zero} = refl
multlemm_ob111 {suc l} {nzero} (x ∷ xs) (x' ∷ xs') [] = refl
multlemm_ob111 {suc l} {suc n} (x ∷ xs) (x' ∷ xs') (y ∷ ys)
  rewrite law10 {y}
  | multlemm_ob111 {suc l} {n} (x ∷ xs) (x' ∷ xs') ys
  | law2 {((x' * x) + foldr _ _+_ zero (zipWith _*_ xs' xs)) }= refl


multlemm_ob11 : {k l n : ℕ}(m1 : k × l)(m2 : k × n)(x : Vec R n)
  → multRow (replicate {_} {l} zero ++ x) (zipWith _++_ m1 m2)
  ≡ multRow x m2
multlemm_ob11 [] [] [] = refl
multlemm_ob11 [] [] (x ∷ xs) = refl
multlemm_ob11 (x ∷ xs) (x' ∷ xs') ( xs0) 
  rewrite multlemm_ob111  x' xs0 x = cong (_∷_ _) (multlemm_ob11 xs  xs' xs0)


multlemm_ob1 : {k l m n : ℕ}(m1 : k × l)(m2 : l × m)
  → zipWith _++_ (replicate {_} {k} (replicate {_} {n} zero)) m1 ⋆
      (replicate {_} {n}  (replicate {_} {m} zero) ++ m2) ≡ m1 ⋆ m2
multlemm_ob1 [] m2 = refl
multlemm_ob1 {suc k} {l} {m} {n} (x ∷ xs) m2
  rewrite 〈〉++◫-lemma (0-mtrx {n} {m} ) m2
  | 〈0-mtrx〉 n m
  | multlemm_ob1 {k} {l} {m} {n} xs m2 
  | multlemm_ob11 (0-mtrx {m} {n}) 〈 m2 〉  x  = cong (_∷_ _) refl --refl


π₂i₂ : (n p : ℕ) → (π₂ {n} {p}) ⋆ (i₂ {n} {p}) ≡ id {p}
π₂i₂ n p 
  rewrite multlemm_ob1 {p} {p} {p} {n} (id {p}) (id {p}) = m-one* (id {p})



{- i₁ ⋆ π₁ ⊹ i₂ ⋆ π₂ ≡ id -}

lemm341 : {k l m n : ℕ}(m1 : k × l)(m2 : m × n)
  → zipWith _++_
      (zipWith _∷_ (replicate  zero) m1 ++
       replicate  (zero ∷ replicate {_} {l} zero))
      (replicate {_} {k} (replicate {_} {n} zero) ++ m2)
  ≡ zipWith _∷_ (replicate zero)  (zipWith _++_ (m1 ++ replicate (replicate zero))
      (replicate {_} {k} (replicate zero) ++ m2))
lemm341 [] [] = refl
lemm341 {nzero} {l} {suc m} {n} [] (x ∷ xs) = cong (_∷_ _) (lemm341 {nzero} {l} {m} {n} [] xs)
lemm341 (x ∷ xs) m2 = cong (_∷_ _) (lemm341 xs m2)

lemm342 : {k l : ℕ}(m1 : k × l)
  → (replicate {_} {k} []) ++++ m1 ≡ m1
lemm342 [] = refl
lemm342 (x ∷ xs) = cong (_∷_ _) (lemm342 xs)

lemm34 :  (k n : ℕ)
  → ((id {k})  ++ (0-mtrx {n} {k})) ++++ ((0-mtrx {k} {n}) ++ (id {n})) ≡ id
lemm34 nzero nzero = refl
lemm34 nzero (suc n) 
  rewrite id-transpose1 (id {n})
  = cong (_∷_ _) (lemm342 (zipWith _∷_ (replicate {_} {n} zero) (id {n})))
lemm34 (suc n) n' 
  rewrite replicatelemm {n} {n'} zero
  | id-transpose1 (id {n})
  | id-transpose1 (id {n'})
  | id-transpose1 (id {n ✚ n'})
  | lemm341 (id {n}) (id {n'})
  | lemm34 n n' = cong (_∷_ _) refl

lemm3 : (n p : ℕ)
  → (i₁ {n} {p}) ⋆ (π₁ {n} {p}) ⊹ (i₂ {n} {p}) ⋆ (π₂ {n} {p}) ≡ id {n ✚ p}
lemm3 n p 
  rewrite concatsup11 (id ++ replicate {_} {p} (replicate {_} {n} zero)) (id {n}) (0-mtrx {n} {p}) 
  | lemm31 (id {n}) (0-mtrx {p} {n}) (id {n})
  | lemm31 (id {n}) (0-mtrx {p} {n}) (0-mtrx {n} {p})
  | concatsup11 (replicate {_} {n} (replicate {_} {p} zero) ++ (id {p})) (0-mtrx {p} {n})  (id {p})
  | lemm31 (0-mtrx {n} {p}) (id {p}) (0-mtrx {p} {n})
  | lemm31 (0-mtrx {n} {p}) (id {p}) (id {p})
  | m-one* (id {p})
  | m-one* (id {n})
  | m-*zero {p} (id {n})
  | m-*zero {n} (id {p})
  | m-*zero {p} (0-mtrx {p} {n})
  | m-*zero {n} (0-mtrx {n} {p})
  | m-zero* {n} (id {p})
  | m-zero* {p} (id {n})
  | lemm32 ((id {n}) ++ (0-mtrx {p} {n})) (replicate {_} {n} (replicate {_} {n} zero) ++ replicate {_} {p} (replicate {_} {n} zero)) 
             (replicate {_} {n}  (replicate {_} {p} zero) ++ replicate {_} {p} (replicate {_} {p} zero)) ((0-mtrx {n} {p}) ++ (id {p}))
  | lemm33 
      (id {n})
      (0-mtrx {n} {n})
      (0-mtrx {p} {n})
      (0-mtrx {p} {n})
  | lemm33
    (0-mtrx {n} {p})
    (0-mtrx {n} {p})
    (0-mtrx {p} {p})
    (id {p})
  | ⊹-zerol
    (0-mtrx {n} {p})
  | ⊹-zerol
    (0-mtrx {p} {n})
  | ⊹-zerol (id {p})
  | ⊹-zeror (id {n})
  = lemm34 n p



{- ⟦ i₁ ∣ i₂ ⟧ ≡ id -}
⟦i₁∣i₂⟧ : (k l : ℕ) → ⟦ i₁ {k} {l} ∣ i₂ {k} {l} ⟧ ≡ id
⟦i₁∣i₂⟧ k l  = lemm3 k l

{-  ⟦ π₁ / π₂ ⟧ ≡ id  -}
⟦π₁/π₂⟧ : (n p : ℕ) → ⟦ π₁ {n} {p} / π₂ {n} {p} ⟧ ≡  id {n ✚ p}
⟦π₁/π₂⟧ k l  = lemm3 k l




