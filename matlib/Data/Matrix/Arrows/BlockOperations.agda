{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings


module Data.Matrix.Arrows.BlockOperations (csr : CommSemiRing) where
open import Data.Vec 
  renaming (zipWith to devnull; map to devnull2)
open import Data.Nat 
  renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality

import Data.Matrix
open Data.Matrix csr

open module csr = CommSemiRing csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.TransposeProperties2; open Data.Matrix.TransposeProperties2 csr
import Data.Matrix.DistributivityProperties; open Data.Matrix.DistributivityProperties csr
import Data.Matrix.MultAssocProperties; open Data.Matrix.MultAssocProperties csr
import Data.Matrix.MultIdentProperties; open Data.Matrix.MultIdentProperties csr
import Data.Matrix.MultZeroProperties; open Data.Matrix.MultZeroProperties csr
import Data.Matrix.AdditionProperties; open Data.Matrix.AdditionProperties csr
import Data.Matrix.Arrows.Identity; open Data.Matrix.Arrows.Identity csr
import Data.Matrix.Arrows.Zero; open Data.Matrix.Arrows.Zero csr

{- C ⋆ ⟦ A ∣ B ⟧ ≡ ⟦ C ⋆ A ∣ C ⋆ B ⟧   -}
m₁⟦m₂|m₃⟧ : {k l m n : ℕ}(A : k × l)(B : k × m)(C : n × k)
  → C ⋆ ⟦ A ∣ B ⟧ ≡ ⟦ C ⋆ A ∣ C ⋆ B ⟧
m₁⟦m₂|m₃⟧ {k} {l} {m} {n} A B C 
  rewrite m-*+distrib C (A ⋆ π₁ {l} {m}) (B ⋆ π₂ {l} {m})
  | m-*assoc C  A (π₁ {l} {m})
  | m-*assoc C  B (π₂ {l} {m})
  = refl


{- ⟦ A / B ⟧ ⋆ C ≡ ⟦ A ⋆ C / B ⋆ C ⟧ -}
⟦m₁/m₂⟧m₃ : {k l m n : ℕ}(A : k × l)(B : m × l)(C : l × n)
  → ⟦ A / B ⟧ ⋆ C ≡ ⟦ A ⋆ C / B ⋆ C ⟧
⟦m₁/m₂⟧m₃ {k} {l} {m} {n} A B C 
  rewrite mm-+*distrib (i₁ {k} {m} ⋆ A) (i₂ {k} {m} ⋆ B) C
  | sym (m-*assoc (i₁ {k} {m}) A C )
  | sym (m-*assoc (i₂ {k} {m}) B C )
  = refl

{- ⟦ R ∣ S ⟧ ⋆ ⟦ U / V ⟧ ≡ R ⋆ U ⊹ S ⋆ V  -}
⟦m₁|m₂⟧⟦m₃/m₄⟧ : {k l m n : ℕ}(R : k × l)(S : k × m)(U : l × n)(V : m × n)
  → ⟦ R ∣ S ⟧ ⋆ ⟦ U / V ⟧ ≡ R ⋆ U ⊹ S ⋆ V
⟦m₁|m₂⟧⟦m₃/m₄⟧ {k} {l} {m} {n} R S U V 
  rewrite m-*+distrib 
      ⟦ R ∣ S ⟧
      (i₁ {l} {m} ⋆ U)
      (i₂ {l} {m} ⋆ V)
  | concatsup11  R (id {l}) (0-mtrx {l} {m} )
  | concatsup11 S (0-mtrx {m} {l}) (id {m})
  | m-*one R
  | m-*one S
  | m-*zero {m} R
  | m-*zero {l} S
  | lemm32 R (0-mtrx {k} {l}) (0-mtrx {k} {m}) S
  | ⊹-zeror R
  | ⊹-zerol S
  | m-*assoc (R ++++ S) i₁  U
  | m-*assoc (R ++++ S) (i₂ {l} {m}) V
  | concatsup212' R S (id {l}) (0-mtrx {m} {l})
  | concatsup212' R S (0-mtrx {l} {m}) (id {m})
  | m-*one R
  | m-*one S
  | m-*zero {m} R
  | m-*zero {l} S
  | ⊹-zeror R
  | ⊹-zerol S
  = refl

