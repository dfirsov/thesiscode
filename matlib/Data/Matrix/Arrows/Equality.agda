{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings


module Data.Matrix.Arrows.Equality (csr : CommSemiRing) where
open import Data.Vec 
  renaming (zipWith to devnull; map to devnull2)
open import Data.Nat 
  renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality

import Data.Matrix
open Data.Matrix csr

open module csr = CommSemiRing csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.TransposeProperties2; open Data.Matrix.TransposeProperties2 csr
import Data.Matrix.DistributivityProperties; open Data.Matrix.DistributivityProperties csr
import Data.Matrix.MultAssocProperties; open Data.Matrix.MultAssocProperties csr
import Data.Matrix.MultIdentProperties; open Data.Matrix.MultIdentProperties csr
import Data.Matrix.MultZeroProperties; open Data.Matrix.MultZeroProperties csr
import Data.Matrix.AdditionProperties; open Data.Matrix.AdditionProperties csr
import Data.Matrix.Arrows.Identity; open Data.Matrix.Arrows.Identity csr
import Data.Matrix.Arrows.Zero; open Data.Matrix.Arrows.Zero csr


{-

 +-         -+      +-                 -+
 | [ A | B ] |      |  +- -+   | +- -+  |
 |           |      |  | A |   | | B |  |
 | --------- |   ≡  |  | - |   | | - |  |
 |           |      |  | C |   | | D |  |
 | [ C | D ] |      |  +- -+   | +- -+  |
 +-         -+      +-                 -+
-}


eq-blocks : {k l m n : ℕ}(A : k × l)(B : k × m)(C : n × l)(D : n × m)
  → ⟦ ⟦ A ∣ B ⟧  /  ⟦ C ∣ D ⟧ ⟧ ≡ ⟦ ⟦ A  /  C ⟧ ∣ ⟦ B / D ⟧ ⟧
eq-blocks {k} {l} {m} {n} A B C D 
  rewrite concatsup11 A (id {l}) (0-mtrx {l} {m})
  | concatsup11 B (0-mtrx {m} {l}) (id {m})
  | concatsup11 C (id {l}) (0-mtrx {l} {m})
  | concatsup11 D (0-mtrx {m} {l}) (id {m})
  | m-*one A
  | m-*one B
  | m-*one C
  | m-*one D
  | m-*zero {m} A
  | m-*zero {l} B
  | lemm32 A (0-mtrx {k} {l}) (0-mtrx {k} {m}) B
  | ⊹-zeror A
  | ⊹-zerol B
  | m-*zero {m} C
  | m-*zero {l} D
  | lemm32 C (0-mtrx {n} {l}) (0-mtrx {n} {m}) D
  | ⊹-zeror C
  | ⊹-zerol D
  | lemm31 (id {k}) (0-mtrx {n} {k}) A
  | lemm31  (0-mtrx {k} {n}) (id {n}) C
  | lemm31 (id {k}) (0-mtrx {n} {k}) B
  | lemm31  (0-mtrx {k} {n}) (id {n}) D
  | m-one* A
  | m-one* B
  | m-one* C
  | m-one* D
  | m-zero* {n} A
  | m-zero* {k} C
  | m-zero* {n} B
  | m-zero* {k} D
  | lemm33 A (0-mtrx {k} {l} ) (0-mtrx {n} {l}) C
  | ⊹-zeror A
  | ⊹-zerol C
  | ⊹-zeror B
  | ⊹-zerol D
  | lemm33 B (0-mtrx {k} {m} ) (0-mtrx {n} {m}) D
  | ⊹-zeror B
  | ⊹-zerol D
  | lemm31 (id {k} ) (0-mtrx {n} {k}) (A ◫ B)
  | m-one* (A ◫ B)
  | m-zero* {n}  (A ◫ B)
  | lemm31 (0-mtrx {k} {n}) (id {n} ) (C ◫ D)
  | m-one* (C ◫ D)
  | m-zero* {k}  (C ◫ D)
  | lemm33 (A ◫ B) 0-mtrx  0-mtrx (C ◫ D)
  | ⊹-zeror (A ◫ B)
  | ⊹-zerol (C ◫ D)
  | concatsup11 (A ++ C) (id {l}) (0-mtrx {l} {m})
  | concatsup11 (B ++ D) (0-mtrx {m} {l}) (id {m})
  | m-*one (B ++ D)
  | m-*one (A ++ C)
  | m-*zero {m} (A ++ C)
  | m-*zero {l} (B ++ D)
  | lemm32 (A ++ C) 0-mtrx 0-mtrx (B ++ D)
  | ⊹-zeror (A ++ C)
  | ⊹-zerol (B ++ D)
  = lemm181 A B C D



{-  
  ⟦ A ∣ B ⟧ ≡ ⟦ C ∣ D ⟧ → A ≡ C & B ≡ D
-}

⟦m₁|m₂⟧≡◫ : {k l m : ℕ} (A : k × l)(B : k × m)
  → ⟦ A ∣ B ⟧ ≡ A ◫ B
⟦m₁|m₂⟧≡◫ {k} {l} {m} A B 
  rewrite concatsup11 A (id {l}) (0-mtrx {l} {m})
  | concatsup11 B (0-mtrx {m} {l}) (id {m})
  | m-*one A
  | m-*one B
  | m-*zero {m} A
  | m-*zero {l} B
  | lemm32 A 0-mtrx 0-mtrx B
  | ⊹-zeror A
  | ⊹-zerol B = refl


lemm1921 : {n : ℕ}{A : Set}(x x0 : A)(xs xs0 : Vec A n)  → x ∷ xs ≡ x0 ∷ xs0 
  → x ≡ x0
lemm1921 x x0 xs ys p = cong (λ q → head q) p

lemm1922 : {k l : ℕ} {A : Set}(x : Vec A k)(x' : Vec A l)(y : Vec A k)(y' : Vec A l)
  → x ++ x' ≡ y ++ y' → x ≡ y
lemm1922 [] x' [] y' p = refl
lemm1922 (x ∷ xs) x' (x0 ∷ xs') y' p 
  rewrite lemm1922 xs x' xs' y' (cong (λ q → tail q) p)
  | lemm1921 x x0 (xs ++ x') (xs' ++ y') p = refl


lemm192 : {k l m : ℕ}(A : k × l)(B : k × m)(C : k × l)(D : k × m)
  → A ◫ B ≡ C ◫ D → A ≡ C
lemm192 [] B [] D p = refl
lemm192 (x ∷ xs) (x' ∷ xs') (x0 ∷ xs0) (x1 ∷ xs1) p 
  with lemm1921 (x ++ x') (x0 ++ x1) (xs ◫ xs') (xs0 ◫ xs1) p | p
... | prf1 | prf2
  rewrite lemm192 xs xs' xs0 xs1 (cong (λ q → tail q) p)
  | lemm1922 x x' x0 x1 prf1 = refl

⟦m₁|m₂⟧≡⟦m₃|m₄⟧ : {k l m : ℕ}(A : k × l)(B : k × m)(C : k × l)(D : k × m)
  → ⟦ A ∣ B ⟧ ≡ ⟦ C ∣ D ⟧ → A ≡ C
⟦m₁|m₂⟧≡⟦m₃|m₄⟧ A B C D p
  rewrite ⟦m₁|m₂⟧≡◫ A B
  | ⟦m₁|m₂⟧≡◫ C D
  | lemm192 A B C D  p = refl


lemm211 : {k l : ℕ}{A : Set}(a : Vec A k)(b c : Vec A l)
  → (a ++ b) ≡ (a ++ c) → b ≡ c
lemm211 [] b c p = p
lemm211 (x ∷ xs) b c p
  rewrite lemm211 xs b c (cong (λ q → tail q) p) = refl

lemm21 : {k l m : ℕ}(B : k × m)(C : k × l)(D : k × m)
  → C ◫ B ≡ C ◫ D → B ≡ D
lemm21 [] [] [] p = refl
lemm21 (x ∷ xs) (x' ∷ xs') (x0 ∷ xs0) p with p
... | prf1
  rewrite lemm21 xs xs' xs0 (cong (λ q → tail q) p)
  with lemm1921 (x' ++ x) (x' ++ x0) (xs' ◫ xs0) (xs' ◫ xs0) prf1 
... | prf2
  rewrite lemm211 x' x x0 prf2 = refl


⟦m₁|m₂⟧≡⟦m₃|m₄⟧' : {k l m : ℕ}(A : k × l)(B : k × m)(C : k × l)(D : k × m)
  → ⟦ A ∣ B ⟧ ≡ ⟦ C ∣ D ⟧ → B ≡ D
⟦m₁|m₂⟧≡⟦m₃|m₄⟧'  {k} {l} {m} A B C D  p with p
... | prf1 
  rewrite ⟦m₁|m₂⟧≡⟦m₃|m₄⟧ A B C D p
  | ⟦m₁|m₂⟧≡◫ C D
  | ⟦m₁|m₂⟧≡◫ C B = lemm21 B C D prf1



{- ⟦ A / B ⟧ ≡ ⟦ C / D ⟧ → A ≡ C & B ≡ D -}

⟦m₁/m₂⟧≡++ : {k l m  : ℕ} (A : k × l)(B : m × l)
  → ⟦ A / B ⟧ ≡ A ++ B
⟦m₁/m₂⟧≡++ {k} {l} {m} A B 
  rewrite lemm31 (id {k}) (0-mtrx {m} {k}) A
  | lemm31 (0-mtrx {k} {m}) (id {m}) B
  | m-one* A
  | m-one* B
  | m-zero* {m} A
  | m-zero* {k} B
  | lemm33 A 0-mtrx 0-mtrx B
  | ⊹-zeror A
  | ⊹-zerol B = refl


⟦m₁/m₂⟧≡⟦m₃/m₄⟧ : {k l m : ℕ}(A : k × l)(B : m × l)(C : k × l)(D : m × l)
  → ⟦ A / B ⟧ ≡ ⟦ C / D ⟧ → A ≡ C
⟦m₁/m₂⟧≡⟦m₃/m₄⟧ {k} {l} {m} A B C D p
  rewrite ⟦m₁/m₂⟧≡++ A B
  | ⟦m₁/m₂⟧≡++ C D = lemm1922 A B C D p


⟦m₁/m₂⟧≡⟦m₃/m₄⟧' : {k l m : ℕ}(A : k × l)(B : m × l)(C : k × l)(D : m × l)
  → ⟦ A / B ⟧ ≡ ⟦ C / D ⟧ → B ≡ D
⟦m₁/m₂⟧≡⟦m₃/m₄⟧' {k} {l} {m} A B C D p
  with p 
... | prf1
  rewrite ⟦m₁/m₂⟧≡++ A B
  | ⟦m₁/m₂⟧≡++ C D
  | lemm1922 A B C D p = lemm211 C B D prf1
