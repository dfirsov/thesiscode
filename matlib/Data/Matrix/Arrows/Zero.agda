{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings

module Data.Matrix.Arrows.Zero (csr : CommSemiRing) where
open import Data.Vec 
  renaming (zipWith to devnull; map to devnull2)
open import Data.Nat 
  renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality

import Data.Matrix
open Data.Matrix csr

open module csr = CommSemiRing csr


import Data.Matrix.MultZeroProperties; open Data.Matrix.MultZeroProperties csr
import Data.Matrix.MultIdentProperties; open Data.Matrix.MultIdentProperties csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr
import Data.Matrix.AdditionProperties; open Data.Matrix.AdditionProperties csr

--  π₁ ⋆ i₂ ≡ 0-mtrx



lemm6 : (n p : ℕ)
  → π₁ {n} {p} ⋆ i₂ {n} {p} ≡ (0-mtrx {n} {p})
lemm6 n p 
  rewrite concatsup212' (id {n}) (0-mtrx {n} {p}) (0-mtrx {n} {p}) (id {p}) 
  | m-zero* {n} (id {p})
  | m-*zero {p} (id {n})
  | ⊹-zerol (0-mtrx {n} {p})
  = refl

--  π₂ ⋆ i₁ ≡ 0-mtrx
lemm7 : (k l : ℕ)
  → π₂ {k} {l} ⋆ i₁ {k} {l} ≡ (0-mtrx {l} {k})
lemm7 n p 
  rewrite concatsup212' (0-mtrx {p} {n}) (id {p}) (id {n}) (0-mtrx {p} {n}) 
  | m-zero* {p} (id {n})
  | m-*zero {n} (id {p})
  | ⊹-zerol (0-mtrx {p} {n})
  = refl

