{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings


module Data.Matrix.Arrows.Extraction (csr : CommSemiRing) where
open import Data.Vec 
  renaming (zipWith to devnull; map to devnull2)
open import Data.Nat 
  renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality

import Data.Matrix
open Data.Matrix csr

open module csr = CommSemiRing csr
import Data.Matrix.HelperLemmas; open Data.Matrix.HelperLemmas csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.TransposeProperties2; open Data.Matrix.TransposeProperties2 csr
import Data.Matrix.DistributivityProperties; open Data.Matrix.DistributivityProperties csr
import Data.Matrix.MultAssocProperties; open Data.Matrix.MultAssocProperties csr
import Data.Matrix.MultIdentProperties; open Data.Matrix.MultIdentProperties csr
import Data.Matrix.MultZeroProperties; open Data.Matrix.MultZeroProperties csr
import Data.Matrix.AdditionProperties; open Data.Matrix.AdditionProperties csr
import Data.Matrix.Arrows.Identity; open Data.Matrix.Arrows.Identity csr
import Data.Matrix.Arrows.Zero; open Data.Matrix.Arrows.Zero csr

⟦m₁|m₂⟧i₁ : {k l m : ℕ}(M₁ : k × l)(M₂ : k × m)
  → ⟦ M₁ ∣ M₂ ⟧ ⋆ (i₁ {l} {m}  ) ≡ M₁
⟦m₁|m₂⟧i₁ {k} {l} {m} A B 
  rewrite concatsup11 A (id {l}) (0-mtrx {l} {m}) 
  | concatsup11 B (0-mtrx {m} {l}) (id {m})
  | m-*one A
  | m-*zero  {m} A 
  | m-*zero  {l} B
  | m-*one B
  | mm-+*distrib (A ++++ 0-mtrx {k} {m}) (0-mtrx {k} {l} ++++ B) (i₁ {l} {m})
  | concatsup212' A (0-mtrx {k} {m}) (id {l}) (0-mtrx {m} {l})
  | concatsup212' (0-mtrx {k} {l}) B (id {l}) (0-mtrx {m} {l})
  | m-*one A
  | m-*one (0-mtrx {k} {l})
  | m-*zero {l} B
  | m-*zero {l} {k} {m} 0-mtrx
  | ⊹-zeror (0-mtrx {k} {l})
  | ⊹-zeror A
  | ⊹-zeror A
    = refl


⟦m₁|m₂⟧i₂ :  {k l m : ℕ}(M₁ : k × l)(M₂ : k × m)
  → ⟦ M₁ ∣ M₂ ⟧ ⋆ (i₂ {l} {m}  ) ≡ M₂
⟦m₁|m₂⟧i₂ {k} {l} {m} A B 
  rewrite concatsup11 A (id {l}) (0-mtrx {l} {m}) 
  | concatsup11 B (0-mtrx {m} {l}) (id {m})
  | m-*one A
  | m-*zero  {m} A 
  | m-*zero  {l} B
  | m-*one B 
  | lemm32 A (0-mtrx {k} {l}) (0-mtrx {k} {m}) B
  | ⊹-zeror A
  | ⊹-zerol B
  | concatsup212' A B  0-mtrx (id {m}) 
  | m-*one B 
  | m-*zero {m} A
  | ⊹-zerol B = refl


π₁⟦m₁/m₂⟧ : {k l m : ℕ}(M₁ : k × l)(M₂ : m × l)
  → π₁ {k} {m} ⋆ ⟦ M₁ / M₂ ⟧ ≡ M₁
π₁⟦m₁/m₂⟧ {k} {l} {m} A B 
  rewrite m-*+distrib (π₁ {k} {m}) (i₁ {k} {m} ⋆ A) (i₂ {k} {m} ⋆ B)
  | m-*assoc (π₁ {k} {m}) (i₁) A 
  | m-*assoc (π₁ {k} {m}) (i₂ {k} {m}) B
  | π₁i₁ k m
  | lemm6 k m
  | m-one* A
  | m-zero* {k} B
  | ⊹-zeror A
  = refl

π₂⟦m₁/m₂⟧ : {k l m : ℕ}(M₁ : k × l)(M₂ : m × l)
  → π₂ {k} {m} ⋆ ⟦ M₁ / M₂ ⟧ ≡ M₂
π₂⟦m₁/m₂⟧ {k} {l} {m} A B
  rewrite m-*+distrib (π₂ {k} {m}) (i₁ {k} {m} ⋆ A) (i₂ {k} {m} ⋆ B) 
  | m-*assoc (π₂ {k} {m}) (i₁) A 
  | m-*assoc (π₂ {k} {m}) (i₂ {k} {m}) B
  | lemm7 k m
  | π₂i₂ k m
  | m-one* B
  | m-zero* {m} A
  | ⊹-zerol B
  = refl