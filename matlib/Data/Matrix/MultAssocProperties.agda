{-# OPTIONS --type-in-type #-}
open import Algebra.CommSemiRings

module Data.Matrix.MultAssocProperties (csr : CommSemiRing) where
open import Data.Vec hiding (zipWith; map)
open import Data.Nat renaming (zero to nzero; _+_ to _✚_; _*_ to _✖_)
open import Relation.Binary.PropositionalEquality
open module csr = CommSemiRing csr

import Data.Matrix; open Data.Matrix csr
import Data.Matrix.TransposeProperties; open Data.Matrix.TransposeProperties csr
import Data.Matrix.TransposeProperties2; open Data.Matrix.TransposeProperties2 csr


-- {- PROPERTY : (a ⋆ b) ⋆ c ≡ a ⋆ (b ⋆ c) -}
m-*assoc811 : (a b c d e f : R)
  → (a * ((b  * c) + e)) + ((d * b) + f) ≡ 
    (((a * c) + d) * b) + ((a * e) + f)
m-*assoc811  a b c d e f
  rewrite law8 {a} {b * c} {e} 
  | law9 {a * c} {d} {b} 
  | law5 {a} {c} {b} 
  | law12 {c} {b} 
  | law1 {(a * (b * c))} {(a * e)} {((d * b) + f)} 
  | law1 {(a * (b * c))} {(d * b)} {((a * e) + f)} 
  | sym (law1 {(a * e)} {(d * b)} {f}) 
  | law4 {(a * e)} {(d * b)} 
  | law1 {d * b} {a * e} {f} = refl


m-*assoc81 : ∀{k m}(x : R)(x' : Vec R k)(xs' : k × m)(xs : Vec R m)(d : Vec R k)
  → (foldr _ _+_ zero 
        (zipWith _*_ (multRow (x ∷ xs) (zipWith _∷_ x' xs')) d))
  ≡  (x * foldr _ _+_ zero (zipWith _*_  d x')) +  
        (foldr _ _+_ zero 
          (zipWith _*_ (multRow xs xs') d))
m-*assoc81 x [] [] _ [] 
  rewrite law11 {x} 
  | law2 {zero} = refl
m-*assoc81 x (x' ∷ xs) (x0 ∷ xs') xs0 (x2 ∷ xs1)
  rewrite 
  m-*assoc811 x x2 x'
    (foldr _ _+_  zero (zipWith _*_ xs0 x0))
    (foldr _ _+_ zero (zipWith _*_ xs1 xs))
    (foldr _ _+_ zero
       (zipWith _*_ (multRow xs0 xs') xs1))
  = cong (_+_ _) (m-*assoc81 x xs xs' xs0 xs1)


m-*assoc83 : (n : ℕ)(d : Vec R n)
  → (foldr _ _+_ zero 
      (zipWith _*_ (multRow [] (replicate {_} {n} []) ) d))
  ≡ zero
m-*assoc83 nzero [] = refl
m-*assoc83 (suc n) (x ∷ xs) rewrite law10 {x} 
  | law2 
    {foldr _ _+_ zero 
      (zipWith _*_ (multRow [] (replicate {_} {n} []) ) xs)}
  = m-*assoc83 n xs


m-*assoc8 : ∀{k m}(m1 : m × k)(v1 : Vec R m)(v2 : Vec R k)
  → (foldr _ _+_ zero
      (zipWith _*_ v1 (multRow v2 m1))) ≡
  (foldr _ _+_ zero 
      (zipWith _*_ (multRow v1 〈 m1 〉) v2))
m-*assoc8 {nzero} [] [] [] = refl
m-*assoc8 {suc n} [] [] d 
  rewrite m-*assoc83 (suc n) d = refl
m-*assoc8 (x' ∷ xs') (x ∷ xs) d 
  rewrite m-*assoc81 x x' 〈 xs' 〉 xs d 
  = cong (_+_ _) (m-*assoc8 xs' xs d)


m-*assocm1 : ∀{m k l}(v : Vec R m)(m1 : l × k)(m2 : m × k)
  → multRow v (m1 ⋆ 〈 m2 〉) ≡ multRow (multRow v 〈 m2 〉) m1
m-*assocm1 [] [] [] = refl
m-*assocm1 (x ∷ xs) [] ys = refl
m-*assocm1 xs (x' ∷ xs') ys 
  rewrite 〈〈m〉〉 ys
  | m-*assoc8 ys xs x'
  = cong (_∷_ _) (m-*assocm1 xs xs' ys)


m-*assocm : {l k m : ℕ}(x : Vec R m)(m2 : m × k )(m3 : k × l)
  → multRow x 〈 m2 ⋆ m3 〉 ≡ 
    multRow (multRow x  〈 m2 〉 ) 〈 m3 〉
m-*assocm {nzero}  [] [] [] = refl
m-*assocm {suc n}  [] [] [] = cong (_∷_ _) (m-*assocm [] [] [])
m-*assocm xs xs' xs0 
  rewrite 〈m₁⋆m₂〉 xs' xs0 
  with 〈 xs0 〉
... | ds = m-*assocm1  xs ds xs'


m-*assoc : {n m k l : ℕ}(m₁ : n × m)(m₂ : m × k)(m₃ : k × l)
  → m₁ ⋆ (m₂ ⋆ m₃) ≡ (m₁ ⋆ m₂) ⋆ m₃
m-*assoc [] m2 m3 = refl
m-*assoc {suc n} {m} {k} {l}  (x ∷ xs) m2 m3 
  rewrite  m-*assocm {l} {k} {m} x m2 m3 
   = cong (_∷_ _) (m-*assoc xs m2 m3)
